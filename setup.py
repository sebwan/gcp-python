from setuptools import setup, find_packages
from configparser import ConfigParser


SETUP_CFG_PATH = 'setup.cfg'

# Instantiate a parser for reading setup.cfg metadata
parser = ConfigParser()
parser.read(SETUP_CFG_PATH)
metadata = dict(parser.items('metadata'))

REQ = [
]

setup(
    name=metadata['name'],
    version=metadata['version'],
    url=metadata['url'],
    description=metadata['short_description'],
    author=metadata['author'],
    author_email=metadata['author_email'],
    license=metadata['license'],
    install_requires=REQ,
    packages=[
       m for m in find_packages() if 'tests' not in m
    ],
)