Python wrapper for Google Cloud Platform
==============================================

.. image:: https://gitlab.com/sebwan/gcp-python/badges/master/build.svg
    :target: https://gitlab.com/sebwan/gcp-python/commits/master
.. image:: https://gitlab.com/sebwan/gcp-python/badges/master/coverage.svg?job=coverage
    :target: https://sebwan.gitlab.io/gcp-python/coverage

See `Documentation <https://sebwan.gitlab.io/gcp-python/>`_